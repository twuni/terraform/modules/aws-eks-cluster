# EKS Cluster | AWS | Terraform Modules | Twuni

This Terraform module provisions an EKS cluster and its supporting resources.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.8 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.60.0 |
| <a name="requirement_tls"></a> [tls](#requirement\_tls) | >= 3.1.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloudwatch_enabled"></a> [cloudwatch\_enabled](#input\_cloudwatch\_enabled) | Enable cluster metrics and logs to be shipped to CloudWatch. | `bool` | `true` | no |
| <a name="input_ec2_capacity_type"></a> [ec2\_capacity\_type](#input\_ec2\_capacity\_type) | The capacity type for EC2 instances, either SPOT or ON\_DEMAND. Use ON\_DEMAND if you want to consume your reserved instances, if any. | `string` | `"SPOT"` | no |
| <a name="input_ec2_instance_types"></a> [ec2\_instance\_types](#input\_ec2\_instance\_types) | The preferred instance types to use for EC2 node groups, if any. | `list(string)` | <pre>[<br>  "t3a.medium",<br>  "t3.medium",<br>  "t2.medium"<br>]</pre> | no |
| <a name="input_ec2_node_count"></a> [ec2\_node\_count](#input\_ec2\_node\_count) | The number of EC2 nodes to provision in this cluster. | `number` | `0` | no |
| <a name="input_fargate_namespaces"></a> [fargate\_namespaces](#input\_fargate\_namespaces) | A list of Kubernetes namespaces in which pods should be executed via AWS Fargate. | `list(string)` | `[]` | no |
| <a name="input_is_public"></a> [is\_public](#input\_is\_public) | Whether the Kubernetes control plane is accessible via the public Internet. | `bool` | `false` | no |
| <a name="input_kubernetes_version"></a> [kubernetes\_version](#input\_kubernetes\_version) | The Kubernetes version this EKS cluster should use. | `string` | `"1.22"` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of this EKS cluster. Subject to EKS cluster naming restrictions. | `string` | `"default"` | no |
| <a name="input_subnet_ids"></a> [subnet\_ids](#input\_subnet\_ids) | The IDs of each subnet in which pods in this cluster will run. | `list(string)` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_ebs_csi_driver_role_arn"></a> [aws\_ebs\_csi\_driver\_role\_arn](#output\_aws\_ebs\_csi\_driver\_role\_arn) | The role ARN to use when annotating the ebs-csi-controller service account in this cluster. |
| <a name="output_aws_lb_controller_role_arn"></a> [aws\_lb\_controller\_role\_arn](#output\_aws\_lb\_controller\_role\_arn) | The role ARN to use when annotating the aws-lb-controller service account in this cluster. |
| <a name="output_cert_manager_role_arn"></a> [cert\_manager\_role\_arn](#output\_cert\_manager\_role\_arn) | The role ARN to use when annotating the cert-manager service account in this cluster. |
| <a name="output_cert_manager_user_name"></a> [cert\_manager\_user\_name](#output\_cert\_manager\_user\_name) | The user name to use for generating an AWS credential (access key) with cert-manager privileges. |
| <a name="output_certificate_authority"></a> [certificate\_authority](#output\_certificate\_authority) | The base64 certificate data required to communicate with this EKS cluster. |
| <a name="output_endpoint"></a> [endpoint](#output\_endpoint) | The Kubernetes API endpoint for this EKS cluster. |
| <a name="output_externaldns_role_arn"></a> [externaldns\_role\_arn](#output\_externaldns\_role\_arn) | The role ARN to use when annotating the external-dns service account in this cluster. |
| <a name="output_externaldns_user_name"></a> [externaldns\_user\_name](#output\_externaldns\_user\_name) | The user name to use for generating an AWS credential (access key) with external-dns privileges. |
| <a name="output_fluent_bit_role_arn"></a> [fluent\_bit\_role\_arn](#output\_fluent\_bit\_role\_arn) | The role ARN to use when annotating the fluent-bit service account in this cluster. |
| <a name="output_oidc_provider_arn"></a> [oidc\_provider\_arn](#output\_oidc\_provider\_arn) | The OpenID Connect provider ARN for this cluster. |
| <a name="output_oidc_provider_url"></a> [oidc\_provider\_url](#output\_oidc\_provider\_url) | The OpenID Connect provider URL for this cluster. |
| <a name="output_security_group_ids"></a> [security\_group\_ids](#output\_security\_group\_ids) | The IDs of the security groups assigned to this EKS cluster. |
