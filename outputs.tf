output "aws_ebs_csi_driver_role_arn" {
  description = "The role ARN to use when annotating the ebs-csi-controller service account in this cluster."
  value       = data.aws_iam_role.irsa["ebs"].arn
}

output "aws_lb_controller_role_arn" {
  description = "The role ARN to use when annotating the aws-lb-controller service account in this cluster."
  value       = data.aws_iam_role.irsa["lb"].arn
}

output "cert_manager_role_arn" {
  description = "The role ARN to use when annotating the cert-manager service account in this cluster."
  value       = data.aws_iam_role.irsa["acme"].arn
}

output "cert_manager_user_name" {
  description = "The user name to use for generating an AWS credential (access key) with cert-manager privileges."
  value       = module.irsa["acme"].user_name
}

output "certificate_authority" {
  description = "The base64 certificate data required to communicate with this EKS cluster."
  value       = aws_eks_cluster.main.certificate_authority
}

output "endpoint" {
  description = "The Kubernetes API endpoint for this EKS cluster."
  value       = aws_eks_cluster.main.endpoint
}

output "externaldns_role_arn" {
  description = "The role ARN to use when annotating the external-dns service account in this cluster."
  value       = data.aws_iam_role.irsa["dns"].arn
}

output "externaldns_user_name" {
  description = "The user name to use for generating an AWS credential (access key) with external-dns privileges."
  value       = module.irsa["dns"].user_name
}

output "fluent_bit_role_arn" {
  description = "The role ARN to use when annotating the fluent-bit service account in this cluster."
  value       = data.aws_iam_role.irsa["o11y"].arn
}

output "oidc_provider_arn" {
  description = "The OpenID Connect provider ARN for this cluster."
  value       = aws_iam_openid_connect_provider.cluster.arn
}

output "oidc_provider_url" {
  description = "The OpenID Connect provider URL for this cluster."
  value       = aws_iam_openid_connect_provider.cluster.url
}

output "security_group_ids" {
  description = "The IDs of the security groups assigned to this EKS cluster."
  value       = [for config in aws_eks_cluster.main.vpc_config : config.cluster_security_group_id]
}
