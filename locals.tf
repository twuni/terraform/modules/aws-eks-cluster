locals {
  cloudwatch_log_group_name = "/aws/eks/${var.name}/cluster"
  service_accounts = {
    acme = {
      name      = "cert-manager"
      namespace = "cert-manager"
    }

    dns = {
      name      = "external-dns"
      namespace = "kube-system"
    }

    ebs = {
      name      = "ebs-csi-controller-sa"
      namespace = "kube-system"
    }

    lb = {
      name      = "aws-load-balancer-controller"
      namespace = "kube-system"
    }

    o11y = {
      name      = "fluent-bit"
      namespace = "kube-system"
    }
  }
  slug = substr(sha256(var.name), 0, 8)
}
