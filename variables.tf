variable "cloudwatch_enabled" {
  default     = true
  description = "Enable cluster metrics and logs to be shipped to CloudWatch."
  type        = bool
}

variable "ec2_capacity_type" {
  default     = "SPOT"
  description = "The capacity type for EC2 instances, either SPOT or ON_DEMAND. Use ON_DEMAND if you want to consume your reserved instances, if any."
  type        = string
}

variable "ec2_instance_types" {
  default = [
    "t3a.medium",
    "t3.medium",
    "t2.medium"
  ]
  description = "The preferred instance types to use for EC2 node groups, if any."
  type        = list(string)
}

variable "ec2_node_count" {
  default     = 0
  description = "The number of EC2 nodes to provision in this cluster."
  type        = number
}

variable "fargate_namespaces" {
  default     = []
  description = "A list of Kubernetes namespaces in which pods should be executed via AWS Fargate."
  type        = list(string)
}

variable "is_public" {
  default     = false
  description = "Whether the Kubernetes control plane is accessible via the public Internet."
  type        = bool
}

variable "kubernetes_version" {
  default     = "1.22"
  description = "The Kubernetes version this EKS cluster should use."
  type        = string
}

variable "name" {
  default     = "default"
  description = "The name of this EKS cluster. Subject to EKS cluster naming restrictions."
  type        = string
}

variable "subnet_ids" {
  description = "The IDs of each subnet in which pods in this cluster will run."
  type        = list(string)
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}
