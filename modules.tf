module "irsa" {
  for_each          = local.service_accounts
  source            = "git::ssh://git@gitlab.com/twuni/terraform/modules/aws-eks-irsa-role.git?ref=main"
  name              = each.value.name
  namespace         = each.value.namespace
  oidc_provider_arn = aws_iam_openid_connect_provider.cluster.arn
  oidc_provider_url = aws_iam_openid_connect_provider.cluster.url
  tags              = var.tags
}
