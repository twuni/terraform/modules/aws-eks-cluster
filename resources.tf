resource "aws_iam_role" "cluster" {
  name_prefix        = "${local.slug}-eks-"
  assume_role_policy = data.aws_iam_policy_document.cluster-arp.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-cluster"
  })
}

resource "aws_iam_role_policy_attachment" "cluster" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster.name
}

resource "aws_iam_role" "fargate" {
  count              = length(var.fargate_namespaces) > 0 ? 1 : 0
  name_prefix        = "${local.slug}-eks-fargate-"
  assume_role_policy = data.aws_iam_policy_document.fargate-arp.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-fargate"
  })
}

resource "aws_iam_role_policy_attachment" "fargate" {
  count      = length(var.fargate_namespaces) > 0 ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"
  role       = aws_iam_role.fargate[count.index].name
}

resource "aws_iam_role" "ec2" {
  count              = var.ec2_node_count > 0 ? 1 : 0
  name_prefix        = "${local.slug}-eks-ec2-"
  assume_role_policy = data.aws_iam_policy_document.ec2-arp.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-ec2"
  })
}

resource "aws_iam_role_policy_attachment" "ec2" {
  count      = var.ec2_node_count > 0 ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.ec2[count.index].name
}

resource "aws_iam_role_policy_attachment" "ec2-networking" {
  count      = var.ec2_node_count > 0 ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.ec2[count.index].name
}

resource "aws_iam_role_policy_attachment" "ec2-registry" {
  count      = var.ec2_node_count > 0 ? 1 : 0
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.ec2[count.index].name
}

resource "aws_iam_policy" "acme" {
  name_prefix = "${local.slug}-eks-acme-"
  path        = "/"
  policy      = data.aws_iam_policy_document.acme.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-acme"
  })
}

resource "aws_iam_role_policy_attachment" "acme" {
  policy_arn = aws_iam_policy.acme.arn
  role       = module.irsa["acme"].role_name
}

resource "aws_iam_user_policy_attachment" "acme" {
  policy_arn = aws_iam_policy.acme.arn
  user       = module.irsa["acme"].user_name
}

resource "aws_iam_policy" "dns" {
  name_prefix = "${local.slug}-eks-dns-"
  path        = "/"
  policy      = data.aws_iam_policy_document.dns.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-dns"
  })
}

resource "aws_iam_role_policy_attachment" "dns" {
  policy_arn = aws_iam_policy.dns.arn
  role       = module.irsa["dns"].role_name
}

resource "aws_iam_user_policy_attachment" "dns" {
  policy_arn = aws_iam_policy.dns.arn
  user       = module.irsa["dns"].user_name
}

resource "aws_iam_policy" "ebs" {
  name_prefix = "${local.slug}-eks-ebs-"
  path        = "/"
  policy      = data.aws_iam_policy_document.ebs.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-ebs"
  })
}

resource "aws_iam_role_policy_attachment" "ebs" {
  policy_arn = aws_iam_policy.ebs.arn
  role       = module.irsa["ebs"].role_name
}

resource "aws_iam_policy" "lb" {
  name_prefix = "${local.slug}-eks-lb-"
  path        = "/"
  policy      = data.aws_iam_policy_document.lb.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-lb"
  })
}

resource "aws_iam_role_policy_attachment" "lb" {
  policy_arn = aws_iam_policy.lb.arn
  role       = module.irsa["lb"].role_name
}

resource "aws_iam_policy" "o11y" {
  name_prefix = "${local.slug}-eks-o11y-"
  path        = "/"
  policy      = data.aws_iam_policy_document.o11y.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-o11y"
  })
}

resource "aws_iam_role_policy_attachment" "o11y" {
  policy_arn = aws_iam_policy.o11y.arn
  role       = module.irsa["o11y"].role_name
}

resource "aws_kms_key" "secrets" {
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  deletion_window_in_days  = 30
  enable_key_rotation      = false
  is_enabled               = true
  key_usage                = "ENCRYPT_DECRYPT"

  tags = merge(var.tags, {
    Name = "${var.name}-eks-secrets"
  })
}

resource "aws_kms_key" "cloudwatch" {
  count                    = var.cloudwatch_enabled ? 1 : 0
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  deletion_window_in_days  = 30
  enable_key_rotation      = false
  is_enabled               = true
  key_usage                = "ENCRYPT_DECRYPT"
  policy                   = data.aws_iam_policy_document.cloudwatch.json

  tags = merge(var.tags, {
    Name = "${var.name}-eks-cloudwatch"
  })
}

resource "aws_eks_cluster" "main" {
  enabled_cluster_log_types = var.cloudwatch_enabled ? [
    "api",
    "audit",
    "authenticator",
    "controllerManager",
    "scheduler"
  ] : []
  name     = var.name
  role_arn = aws_iam_role.cluster.arn
  version  = var.kubernetes_version

  encryption_config {
    provider {
      key_arn = aws_kms_key.secrets.arn
    }
    resources = ["secrets"]
  }

  kubernetes_network_config {
    service_ipv4_cidr = "172.16.0.0/12"
  }

  vpc_config {
    endpoint_private_access = !var.is_public
    endpoint_public_access  = var.is_public
    public_access_cidrs     = var.is_public ? ["0.0.0.0/0"] : []
    security_group_ids      = []
    subnet_ids              = var.subnet_ids
  }

  tags = merge(var.tags, {
    Name = var.name
  })

  depends_on = [
    aws_iam_role_policy_attachment.cluster
  ]
}

resource "aws_cloudwatch_log_group" "cluster" {
  count             = var.cloudwatch_enabled ? 1 : 0
  kms_key_id        = aws_kms_key.cloudwatch[0].arn
  name              = local.cloudwatch_log_group_name
  retention_in_days = 7

  tags = merge(var.tags, {
    Name = var.name
  })
}

resource "aws_cloudwatch_log_stream" "containers" {
  count          = var.cloudwatch_enabled ? 1 : 0
  name           = "containers"
  log_group_name = aws_cloudwatch_log_group.cluster[0].name
}

resource "aws_eks_node_group" "primary" {
  count           = var.ec2_node_count > 0 ? 1 : 0
  ami_type        = "AL2_x86_64"
  capacity_type   = var.ec2_capacity_type
  cluster_name    = aws_eks_cluster.main.name
  disk_size       = 20
  instance_types  = var.ec2_instance_types
  node_group_name = "${var.name}-${count.index}-${substr(sha256(join(",", var.ec2_instance_types)), 0, 8)}"
  node_role_arn   = aws_iam_role.ec2[count.index].arn
  release_version = data.aws_ssm_parameter.node_release_version.value
  subnet_ids      = var.subnet_ids

  scaling_config {
    desired_size = var.ec2_node_count
    max_size     = var.ec2_node_count
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.ec2,
    aws_iam_role_policy_attachment.ec2-networking,
    aws_iam_role_policy_attachment.ec2-registry,
  ]

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [scaling_config[0].desired_size]
  }
}

resource "aws_eks_fargate_profile" "namespaces" {
  for_each               = toset(var.fargate_namespaces)
  cluster_name           = aws_eks_cluster.main.name
  fargate_profile_name   = each.value
  pod_execution_role_arn = aws_iam_role.fargate[0].arn
  subnet_ids             = var.subnet_ids

  selector {
    labels    = {}
    namespace = each.value
  }

  tags = merge(var.tags, {
    Name = each.value
  })
}

resource "aws_iam_openid_connect_provider" "cluster" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.cluster.certificates[0].sha1_fingerprint]
  url             = aws_eks_cluster.main.identity[0].oidc[0].issuer
}
